import { configureStore } from "@reduxjs/toolkit";
import tokenReducer from "./modules/tokenStore";
const store=configureStore(
    {
        reducer:{
            tokenStore:tokenReducer
        }
    }
)
export default store