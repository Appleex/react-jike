import { loginAPI } from "@/apis/user"
import logo from "@/assets/logo.png"
import { setToken } from "@/store/modules/tokenStore"
import { Button, Card, Form, Input, message } from 'antd'
import { useDispatch } from "react-redux"
import { useNavigate } from "react-router-dom"
import './index.scss'

const Login = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  //点击登录按钮的处理处理
  const onFinish = (value) => {
    loginAPI(value).then(
      res => {
        const token = res.data.data.token
        if (token) {
          dispatch(setToken(token))
          navigate("/")
          message.success("登录成功")
        } 
      }
    )


  }
  return (
    <div className="login">
      <Card className="login-container">
        <img className="login-logo" src={logo} alt="" />
        {/* 登录表单 */}
        <Form validateTrigger="onBlur" onFinish={onFinish}>
          <Form.Item
            name="mobile"
            // 多条校验逻辑 先校验第一条 第一条通过之后再校验第二条
            rules={[
              {
                required: true,
                message: "请输入手机号"
              },
              {
                pattern: /^1[3-9]\d{9}$/,
                message: "请输入正确的手机号码格式"
              }
            ]}
          >
            <Input size="large" placeholder="请输入手机号" />
          </Form.Item>
          <Form.Item
            name="code"
            rules={[
              {
                required: true
                ,
                message: "请输入密码"
              }
            ]}
          >
            <Input size="large" placeholder="请输入验证码" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" size="large" block >
              登录
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  )
}

export default Login