import { commitAPI } from '@/apis/article'
import { useChannels } from "@/hooks/useChannels"
import { PlusOutlined } from '@ant-design/icons'
import {
    Breadcrumb,
    Button,
    Card,
    Form,
    Input,

    Radio,
    Select,
    Space,
    Upload,
    message
} from 'antd'
import { useRef, useState } from 'react'
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'
import { Link } from 'react-router-dom'
import './index.scss'
const Publish = () => {
const {channelList}=useChannels()
    //提交按钮处理函数
    const onFinish = (formValue) => {
        //收集表单数据
        const { title, content, channel_id } = formValue
        var reqData = {
            title, content, channel_id,
            cover: {
                type: type,
                image: fileList.map(item => item.response.data.url)
            }
        }
        //校验图片数量是否符合类型
        if (fileList.length !== type) return message.warning("图片类型和封面不匹配")
        //调用api提交后端
        commitAPI(reqData).then(res => {
            message.success("提交成功")
        
        })

    }
    //图片上上传处理函数
    const [fileList, setFileList] = useState([])
    const onChange = (value) => {
        setFileList(value.fileList)
    }
    //切换封面处理函数
    const [type, setType] = useState(0)
    const typeChange = (e) => {
        setType(e.target.value)
    }
const formRef=useRef()
    return (
        <div className="publish">
            <Card
                title={
                    <Breadcrumb items={[
                        { title: <Link to={'/'}>首页</Link> },
                        { title: "文章" },
                    ]}
                    />
                }
            >
                <Form
                ref={formRef}
                    labelCol={{ span: 4 }}
                    wrapperCol={{ span: 16 }}
                    initialValues={{ type: 0 }}
                    onFinish={onFinish}
                   

                >
                    <Form.Item
                        label="标题"
                        name="title"
                        rules={[{ required: true, message: '请输入文章标题' }]}
                    >
                        <Input placeholder="请输入文章标题" style={{ width: 400 }} />
                    </Form.Item>
                    <Form.Item
                        label="频道"
                        name="channel_id"
                        rules={[{ required: true, message: '请选择文章频道' }]}
                    >
                        <Select placeholder="请选择文章频道" style={{ width: 400 }}>
                            {/* value属性用户选中之后会自动收集起来作为接口的提交字段 */}
                            {channelList.map(item => <option key={item.id} value={item.id}>{item.name}</option>)}
                        </Select>
                    </Form.Item>
                    <Form.Item label="封面">
                        <Form.Item name="type">
                            <Radio.Group onChange={typeChange} >
                                <Radio value={1}>单图</Radio>
                                <Radio value={3}>三图</Radio>
                                <Radio value={0}>无图</Radio>
                            </Radio.Group>
                        </Form.Item>
                        {/* 
              listType: 决定选择文件框的外观样式
              showUploadList: 控制显示上传列表
            */}
                        {type > 0 && <Upload
                            listType="picture-card"
                            showUploadList
                            action={'http://geek.itheima.net/v1_0/upload'}
                            name='image'
                            onChange={onChange}
                            maxCount={type}
                        >
                            <div style={{ marginTop: 8 }}>
                                <PlusOutlined />
                            </div>
                        </Upload>}

                    </Form.Item>

                    <Form.Item
                        label="内容"
                        name="content"
                        rules={[{ required: true, message: '请输入文章内容' }]}
                    >
                        {/* 富文本编辑器 */}
                        <ReactQuill
                            className="publish-quill"
                            theme="snow"
                            placeholder="请输入文章内容"></ReactQuill>
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 4 }}>
                        <Space>
                            <Button size="large" type="primary" htmlType="submit">
                                发布文章
                            </Button>
                        </Space>
                    </Form.Item>
                </Form>
            </Card>
        </div>
    )
}

export default Publish