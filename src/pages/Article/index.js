import { Breadcrumb, Button, Card, DatePicker, Form, Popconfirm, Radio, Select } from 'antd'
import { Link } from 'react-router-dom'
// 引入汉化包 时间选择器显示中文
import locale from 'antd/es/date-picker/locale/zh_CN'

// 导入资源
import { getArticleListAPI } from '@/apis/article'
import img404 from '@/assets/error.png'
import { useChannels } from '@/hooks/useChannels'
import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import { Space, Table, Tag } from 'antd'
import { useEffect, useState } from 'react'

const { Option } = Select
const { RangePicker } = DatePicker

const Article = () => {
    const { channelList } = useChannels()

    // 准备列数据
    // 定义状态枚举
    const status = {
        1: <Tag color='warning'>待审核</Tag>,
        2: <Tag color='success'>审核通过</Tag>,
    }
    const columns = [
        {
            title: '封面',
            dataIndex: 'cover',
            width: 120,
            render: cover => {
                return <img src={cover.images[0] || img404} width={80} height={60} alt="" />
            }
        },
        {
            title: '标题',
            dataIndex: 'title',
            width: 220
        },
        {
            title: '状态',
            dataIndex: 'status',
            // data - 后端返回的状态status 根据它做条件渲染
            // data === 1 => 待审核
            // data === 2 => 审核通过
            render: data => status[data]
        },
        {
            title: '发布时间',
            dataIndex: 'pubdate'
        },
        {
            title: '阅读数',
            dataIndex: 'read_count'
        },
        {
            title: '评论数',
            dataIndex: 'comment_count'
        },
        {
            title: '点赞数',
            dataIndex: 'like_count'
        },
        {
            title: '操作',
            render: data => {
                return (
                    <Space size="middle">
                        <Button type="primary" shape="circle" icon={<EditOutlined />} />
                        <Popconfirm
                            title="删除文章"
                            description="确认要删除当前文章吗?"

                            okText="Yes"
                            cancelText="No"
                        >
                            <Button
                                type="primary"
                                danger
                                shape="circle"
                                icon={<DeleteOutlined />}
                            />
                        </Popconfirm>
                    </Space>
                )
            }
        }
    ]
    //准备筛选条件
    const [reqDate, setReqDate]=useState({
        status:'',
        channel_id:'',
        begin_pubdate:'',
        end_pubdate:'',
        page:1,
        per_page:4
    })
    //获取表单中的筛选条件
   const onFish=(formValue)=>{
      setReqDate(
        {
            ...reqDate,
            channel_id:formValue.channel_id,
            status:formValue.status,
            begin_pubdate:formValue.date[0].format("YYYY-MM-DD"),
            end_pubdate:formValue.date[1].format("YYYY-MM-DD")
        }

      )
   }
    //调用api获取文章数据 
    const [articleList, setArticleList] = useState([])
    const [total, setTotal] = useState(0)
    useEffect(() => {
        getArticleListAPI(reqDate).then(res => {
            setArticleList(res.data.data.results)
            setTotal(res.data.data.total_count)
        })
    }, [reqDate])
   const pageChange=(page)=>{
    setReqDate({
        ...reqDate,
        page:page

    })
   }
    return (
        <div>
            <Card
                title={
                    <Breadcrumb items={[
                        { title: <Link to={'/'}>首页</Link> },
                        { title: '文章列表' },
                    ]} />
                }
                style={{ marginBottom: 20 }}
            >
                <Form initialValues={{ status: '' }} onFinish={onFish} >
                    <Form.Item label="状态" name="status" >
                        <Radio.Group>
                            <Radio value={''}>全部</Radio>
                            <Radio value={1}>待审核</Radio>
                            <Radio value={2}>审核通过</Radio>
                        </Radio.Group>
                    </Form.Item>

                    <Form.Item label="频道" name="channel_id">
                        <Select
                            placeholder="请选择文章频道"
                            style={{ width: 120 }}
                        >
                            {channelList.map(item => <Option key={item.id} value={item.id}>{item.name}</Option>)}
                        </Select>
                    </Form.Item>

                    <Form.Item label="日期" name="date">
                        {/* 传入locale属性 控制中文显示*/}
                        <RangePicker locale={locale}></RangePicker>
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" style={{ marginLeft: 40 }} >
                            筛选
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
            {/* 表格区域 */}
            <Card title={`根据筛选条件共查询到${total}条结果：`}>
                <Table rowKey="id" columns={columns} dataSource={articleList} pagination={{
                    pageSize:reqDate.per_page,
                    total:total,
                    onChange:pageChange
                }} />
            </Card>
        </div>
    )
}

export default Article