const TOKENKEY="token_key"
//获取token
const getToken=()=>{
   return localStorage.getItem(TOKENKEY)
}
//存储token
const setToken=(token)=>{
     localStorage.setItem(TOKENKEY,token)
 }
 //删除token
 const removeToken=()=>{
  localStorage.removeItem(TOKENKEY)
 }
 export {
    getToken, removeToken, setToken
}

