import router from "@/router";
import axios from "axios";
import { getToken, removeToken } from "./token";
const request = axios.create({
  //配置基础url
  baseURL: 'http://geek.itheima.net/v1_0',
  //配置超时时间
  timeout: 5000,

});
// 添加请求拦截器
request.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  const token=getToken()
  if (token) {  config.headers.Authorization = `Bearer ${token}`
 }
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
request.interceptors.response.use(function (response) {
  // 2xx 范围内的状态码都会触发该函数。
  // 对响应数据做点什么
  return response;
}, function (error) {
  // 超出 2xx 范围的状态码都会触发该函数。
  // 对响应错误做点什么
  //监控接口返回的401状态码
  if(error.response.status===401){
    //清除失效的token
    removeToken()
    //跳转登录页
    router.navigate("/login")
    window.location.reload()
  }
  return Promise.reject(error);
});
export { request };

