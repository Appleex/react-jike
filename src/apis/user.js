import { request } from "@/utils"

const loginAPI=(userInfo)=>{
   return request({
        url:"/authorizations",
        method:"POST",
        data:userInfo
    })
}
const getUserInfoAPI=()=>{
    return request({
         url:"/user/profile",
         method:"GET",
    
     })
 }
 export { getUserInfoAPI, loginAPI }
