import { request } from "@/utils"

const getChannelListAPI = () => {
    return request({
        url: "/channels",
        method: "GET",

    })
}
const commitAPI = (data) => {
    return request({
        url: "/mp/articles?draft=false",
        method: "POST",
        data: data

    })
}
//获取文章列表
const getArticleListAPI = (params) => {
    return request(
        {
            url: "mp/articles",
            method: "GET",
            params
        }
    )
}
export { commitAPI, getArticleListAPI, getChannelListAPI }

