
import { AuthRouter } from "@/components/AuthRouter"
import Article from "@/pages/Article"
import Home from "@/pages/Home"
import Layout from "@/pages/Layout"
import Publish from "@/pages/Publish"
import { createBrowserRouter } from "react-router-dom"
import Login from "../pages/Login"
const router = createBrowserRouter([
    {
        path: "/",
        element: <AuthRouter><Layout></Layout></AuthRouter>,
        children: [
            {
                index: true,
                element: <Home></Home>
            },
            {
                path: "/article",
                element: <Article></Article>
            },
            {
                path: "/publish",
                element: <Publish></Publish>
            }
        ]
    },
    {
        path: "/login",
        element: <Login></Login>
    }
])
export default router