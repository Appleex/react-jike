import { getChannelListAPI } from "@/apis/article"
import { useEffect, useState } from "react"
const useChannels=()=>{
        //接受后端查询到的频道列表
        const [channelList, setChannelList] = useState([])
        //调用api获取channellist
        useEffect(() => {
            getChannelListAPI().then(
                res => setChannelList(res.data.data.channels)
            )
        }, [])
        return {
            channelList
        }
}
export { useChannels }
